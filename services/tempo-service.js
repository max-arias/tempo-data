const rp     = require('request-promise');
const _      = require('lodash');
const extend = require('util')._extend;

const baseOptions = {
  url: 'https://api.tempo.io/2',
  headers: {
    'Authorization': `Bearer ${process.env.TEMPO_TOKEN}`
  }
};

const errorHandler = (error) => {
  return {
    error: true,
    name: error.name,
    statusCode: error.statusCode,
    message: error.message
  }
};

const makeRequest = async (options) => {
  const response = await rp(options).catch(errorHandler);
  if (response.error) {
    return response;
  }
  return JSON.parse(response);
};

const getMemberData = async (teamId, fromDate, toDate) => {
  const options = { ...baseOptions };
  options.url   = `${options.url}/teams/${teamId}/members`;

  const teamUsers        = await makeRequest(options);
  const teamUserMap      = [];
  const teamUserPromises = [];
  const teamUsersOut     = [];

  teamUsers.results.map(async (memberResult) => {
    const username     = memberResult.member.username;
    const extraOptions = {
      from: fromDate,
      to: toDate,
    };

    if (username) {
      const userPlanPromise = findUserPlan(username, extraOptions);
      teamUserPromises.push(userPlanPromise);
    }
  });

  const resolvedPromises = await Promise.all(teamUserPromises);

  resolvedPromises.map((memberInfoResult, resolvedIndex) => {
    const memberResult = teamUsers.results[resolvedIndex];

    // A user can have multiple plans
    memberInfoResult.results.map((plan) => {
      const username           = _.get(memberResult, ['member', 'username'], '-');
      const membershipToDate   = _.get(memberResult, ['memberships', 'active', 'to'], '-');
      const membershipRoleName = _.get(memberResult, ['memberships', 'active', 'role', 'name'], '-');

      const newMember = {
        username,
        toDate:   membershipToDate,
        endDate:  plan.endDate,
        roleName: membershipRoleName
      };

      teamUserMap.push(newMember);
    }, this);
  }, this);

  return teamUserMap;
};

const findAllTeams = async () => {
  const options = { ...baseOptions };
  options.url = `${options.url}/teams`;

  return await makeRequest(options);
};

const findTeamMembers = async (teamId) => {
  const options = { ...baseOptions };
  options.url = `${options.url}/teams/${teamId}/members`;

  return await makeRequest(options);
};

const findUserPlan = async (userName, extraOptions) => {
  const options = { ...baseOptions };
  options.url = `${options.url}/plans/user/${userName}`;
  options.qs  = extraOptions;

  return await makeRequest(options);
}

module.exports = {
  findAllTeams,
  findTeamMembers,
  findUserPlan,
  getMemberData
}
