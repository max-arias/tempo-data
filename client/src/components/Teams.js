import React, { PureComponent } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import CircularProgress from '@material-ui/core/CircularProgress';
import teal from '@material-ui/core/colors/teal';
import red from '@material-ui/core/colors/red';

export default class Teams extends PureComponent {
  state = {
    teams: [],
    error_message: '',
  }

  async componentDidMount() {
    const response = await fetch('/api/teams');
    const teams = await response.json();

    if (teams.error) {
      this.setState({ error_message: 'There was an error loading "Teams"' });
    } else {
      this.setState({ teams: teams.results, error_message: '' });
    }
  }

  render() {
    const { teams, error_message } = this.state;
    const { onSelect } = this.props;

    return (
      <div>
        <h1 className="section-header">Teams</h1>
          {error_message !== '' ? (
              <h2 className="error">{ error_message }</h2>
          ) : ''}

          { !teams.length && error_message === '' ? (
            <CircularProgress size={50} style={{ color: teal['A200'] }}/>
          ) : (
            <List>
              { teams.map((value, index) => (
                <ListItem
                  button
                  onClick={() => {
                    onSelect(value.id);
                    return false;
                  }}
                  key={value.id}
                >
                  <ListItemText primary={value.name} />
                </ListItem>
              )) }
            </List>
          )}
      </div>
    )
  }
}
