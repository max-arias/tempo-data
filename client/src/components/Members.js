import axios from 'axios';
import MUIDataTable from "mui-datatables";
import CircularProgress from '@material-ui/core/CircularProgress';
import teal from '@material-ui/core/colors/teal';
import React, { PureComponent } from 'react';


const options = {
  filterType: 'checkbox',
  pagination: false,
  rowsPerPage: 999999
};

const columns = [
  {
    name: "Username",
    options: {
      filter: true,
      sort: true,
    }
  },
  {
    name: "To Date",
    options: {
      filter: false,
      sort: true,
    }
  },
  {
    name: "Planning End Date",
    options: {
      filter: false,
      sort: true,
    }
  },
  {
    name: "Role Name",
    options: {
      filter: true,
      sort: true,
    }
  },
];
 
export default class Members extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      teamId: this.props.teamId,
      fromDate: this.props.from,
      toDate: this.props.to,
      retrieve: this.props.retrieve,
      members: [],
    }

    this.loadMembers = this.loadMembers.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.fromDate !== prevState.fromDate || nextProps.toDate !== prevState.toDate) {
      return {
        fromDate: nextProps.from,
        toDate: nextProps.to,
        retrieve: nextProps.retrieve,
        members: []
      };
    }

    return null;
  }

  componentDidUpdate() {
    const { fromDate, toDate, retrieve } = this.state;
    if (fromDate && toDate && retrieve) {
      if (this.state.retrieve) {
        this.setState({ retrieve: false });
        this.loadMembers();
      }      
    }
  }

  async loadMembers() {
    const { teamId, fromDate, toDate, retrieve } = this.state;

    if (retrieve) {
      const memberInfo = await axios.post('/api/member-info', {
        teamId: teamId,
        fromDate: fromDate,
        toDate: toDate,
      });

      // Convert from array of objects, to array of arrays for mui datatables
      const convertedMembers = memberInfo.data.map((m) => {
        return [m.username || '', m.toDate || '', m.endDate || '', m.roleName || ''];
      });

      console.log(convertedMembers);
  
      this.setState({ members: convertedMembers, retrieve: false });
    }
  }

  async componentDidMount() {
    this.loadMembers();
  }

  render() {
    const { members } = this.state;

    return (
      <div>
        <h1 className="section-header">Members</h1>
          { !members.length ? (
            <CircularProgress size={50} style={{ color: teal['A200'] }}/>
          ) : (
            <MUIDataTable 
              data={ members } 
              columns={ columns } 
              options={ options } 
            />
          )}
      </div>
    );
  }
}