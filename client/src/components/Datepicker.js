import React, { PureComponent } from 'react';
import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';

import DatePicker from 'material-ui-pickers/DatePicker';

export default class App extends PureComponent {
  state = {
    selectedDate: this.props.date || new Date(),
  }

  handleDateChange = (date) => {
    this.props.onDateChange(date);
    this.setState({ selectedDate: date });
  }

  render() {
    const { selectedDate } = this.state;
    const { label } = this.props;

    return (
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <div className="date-picker">
          <DatePicker
            label={label}
            value={selectedDate}
            onChange={this.handleDateChange}
            autoOk
          />
        </div>
      </MuiPickersUtilsProvider>
    );
  }
}
