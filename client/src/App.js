import React, { Component } from 'react';
import moment from 'moment';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import './App.css';

import Datepicker from './components/Datepicker';
import Teams from './components/Teams';
import Members from './components/Members';

class App extends Component {
  constructor(props) {
    super(props);

    const currentDate = moment().format('YYYY-MM-DD');
    const futureDate  = moment().add(40, 'days').format('YYYY-MM-DD');
    
    this.state = {
      selectedTeam: null,
      fromDate: currentDate,
      toDate: futureDate,
      retrieve: true
    }

    this.onTeamSelect = this.onTeamSelect.bind(this);
    this.onDateToSelect = this.onDateToSelect.bind(this);
    this.onDateFromSelect = this.onDateFromSelect.bind(this);
  }

  onTeamSelect (teamId) {
    this.setState({ selectedTeam: teamId });
  }

  onDateFromSelect (date) {
    this.setState({ fromDate: date.format('YYYY-MM-DD'), retrieve: true });
  }

  onDateToSelect (date) {
    this.setState({ toDate: date.format('YYYY-MM-DD'), retrieve: true });
  }

  render() {
    const { selectedTeam, fromDate, toDate, retrieve } = this.state;

    return (
      <div className="App">
        <Grid container spacing={24}>
          <Grid item xs></Grid>
          <Grid item xs={8}>
            <Paper>
              <Teams
                onSelect={this.onTeamSelect}
              />
            </Paper>
          </Grid>
          <Grid item xs></Grid>
        </Grid>

        { selectedTeam ? (
          <Grid container spacing={24}>
            <Grid item xs></Grid>
            <Grid item xs={8}>
              <Paper>
                <Grid container spacing={24}>
                  <Grid item xs></Grid>
                  <Grid item xs={4}>
                    <Datepicker
                      className="fromPicker"
                      label="From"
                      date={fromDate}
                      onDateChange={ this.onDateFromSelect }
                    />                    
                  </Grid>
                  <Grid item xs={4}>
                    <Datepicker
                      className="toPicker"
                      label="To"
                      date={toDate}
                      onDateChange={ this.onDateToSelect }
                    />
                  </Grid>
                  <Grid item xs></Grid>
                </Grid>
                <Members
                  teamId={ selectedTeam }
                  from={ fromDate }
                  to={ toDate }
                  retrieve={ retrieve }
                />
              </Paper>
            </Grid>
            <Grid item xs></Grid>
          </Grid>
        ) : '' }
      </div>
    );
  }
}

export default App;
