var express = require('express');
var router = express.Router();

const { findUserPlan } = require("../services/tempo-service");

/* GET user planning. */
router.get('/user/:username', async (req, res, next) => {
  const options = {
    from: req.query.from, // Format is: YYYY-MM-DD
    to: req.query.to
  }
  const userPlan = await findUserPlan(req.params.username, options);
  res.json(userPlan);
});

module.exports = router;
