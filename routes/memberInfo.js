var express = require('express');
var router = express.Router();

const { getMemberData } = require("../services/tempo-service");

/* POST to retrieve member data */
router.post('/', async (req, res, next) => {
  const teamId   = req.body.teamId;
  const fromDate = req.body.fromDate;
  const toDate   = req.body.toDate;

  const memberData = await getMemberData(teamId, fromDate, toDate);

  res.json(memberData);
});

module.exports = router;
