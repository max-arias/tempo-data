const express = require('express');
const { findAllTeams, findTeamMembers } = require("../services/tempo-service");

const router = express.Router();

/* GET teams listing. */
router.get('/', async (req, res, next) => {
  const teams = await findAllTeams();
  res.json(teams);
});

/* GET members listing. */
router.get('/:id/members', async (req, res, next) => {
  const teamMembers = await findTeamMembers(req.params.id);
  res.json(teamMembers);
});

module.exports = router;
